# Papirus Colorizer for Pywal
Matches Papirus and Papirus-Dark gtk directory icons with Pywal generated colorscheme.

# Dependencies
- pywal theme in `~/.cache/wal/colors`
- [papirus-icon-theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
- `rsync`

# Usage
1. generate pywal theme
2. call `./colorize`
3. use something like [wpg](https://github.com/deviantfero/wpgtk) to generate matching gtk theme (optional)
4. refresh gtk theme (reload in settings or `wpg` to do it for you each time)

First use requires `root` password to change ownership of `/usr/share/icons/{Papirus,Papirus-Dark}` to current user.
